import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import List from '@/components/List'
import Post from '@/components/Post'
import Olahraga from '@/components/Olahraga'
import Travell from '@/components/Travell'
Vue.use(Router)
export default new Router({
    routes: [
        {
            path: '/',
            name: 'Root',
            component: Home
        },
        {
            path: '/list',
            name: 'List',
            component: List
        },
        {
            path: '/Post',
            name: 'Post',
            component: Post
        },
        {
          path:'/Olahraga',
          name:'Olahraga',
          component:Olahraga
        },
        {
          path:'/Travell',
          name:'Travell',
          component:Travell
        }
    ],
    mode: 'history'
})